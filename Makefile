SHELL = /bin/bash
pre-install:;
	mkdir -p ./config/mariadb/initdb.d
	[ -f ./config/mariadb/initdb.d/eclipse_accounts_shared.sql ] && echo "EF shared DB dump already exists, skipping fetch" || (scp api-vm1:~/webdev/sql/eclipse_accounts_shared.sql.gz ./config/mariadb/initdb.d/ && gzip -d ./config/mariadb/initdb.d/eclipse_accounts_shared.sql.gz )

dev-start:;
	mvn compile -e quarkus:dev -Dconfig.secret.path=$$PWD/config/application/secret.properties

clean:;
	mvn clean

install-yarn:;
	yarn install --frozen-lockfile --audit

compile-test-resources: install-yarn;
	yarn run generate-json-schema

compile-java: compile-test-resources;
	mvn compile package

compile-java-quick: compile-test-resources;
	mvn compile package -Dmaven.test.skip=true

compile: clean compile-java;

compile-quick: clean compile-java-quick;

compile-start: compile-quick;
	docker compose down
	docker compose build
	docker compose up -d
