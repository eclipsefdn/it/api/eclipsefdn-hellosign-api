/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.services.impl;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.hellosign.model.BadStateNotificationParams;
import org.eclipsefoundation.hellosign.services.MailerService;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import io.quarkus.qute.Location;
import io.quarkus.qute.Template;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 * Default basic mailer that will send a notification to the configured address for error states in the Hellosign
 * workflow.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DefaultMailerService implements MailerService {

    @ConfigProperty(name = "eclipse.mailer.hellosign.notification-address", defaultValue = "emo-records@eclipse.org")
    String notificationAddress;

    @Inject
    Mailer mailer;

    @Location("MailerService/emo_notification")
    Template emoNotificationTemplate;

    @Override
    public void sendMailToEMO(BadStateNotificationParams params) {
        Mail m = Mail
                .withText(notificationAddress,
                        String
                                .format("Hellosign Document for an %s currently requires your attention",
                                        params.getDocumentType()),
                        emoNotificationTemplate.data("params", params).render());

        mailer.send(m);
    }

}
