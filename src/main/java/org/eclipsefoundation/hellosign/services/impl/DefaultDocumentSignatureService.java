/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.services.impl;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;
import org.eclipsefoundation.efservices.services.ProfileService;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationDocumentDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysModLogDataBuilder;
import org.eclipsefoundation.hellosign.apis.FoundationDbAPI;
import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration;
import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration.DocumentReference;
import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration.DocumentSignatureTemplate;
import org.eclipsefoundation.hellosign.daos.EclipsePersistenceDao;
import org.eclipsefoundation.hellosign.dtos.docsign.DocumentSigner;
import org.eclipsefoundation.hellosign.dtos.docsign.SignatureRequest;
import org.eclipsefoundation.hellosign.dtos.eclipse.AccountRequests;
import org.eclipsefoundation.hellosign.dtos.eclipse.AccountRequests.AccountRequestsCompositeId;
import org.eclipsefoundation.hellosign.helpers.ConversionHelpers;
import org.eclipsefoundation.hellosign.model.BadStateNotificationParams;
import org.eclipsefoundation.hellosign.model.EfHellosignRequestData;
import org.eclipsefoundation.hellosign.model.HellosignSignatureRequest;
import org.eclipsefoundation.hellosign.namespaces.HellosignRequestStatus;
import org.eclipsefoundation.hellosign.namespaces.SignatureAPIParameterNames;
import org.eclipsefoundation.hellosign.services.DocumentSignatureService;
import org.eclipsefoundation.hellosign.services.HellosignBindingService;
import org.eclipsefoundation.hellosign.services.MailerService;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.jboss.resteasy.reactive.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dropbox.sign.model.SignatureRequestResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.ServerErrorException;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

/**
 * Default implementation for document signature workflows, integrating with Hellosign and the persisted request records as a backing store.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DefaultDocumentSignatureService implements DocumentSignatureService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDocumentSignatureService.class);

    private static final String API_MODLOG_AUTHOR = "api";
    private static final String API_MODLOG_ACTION = "UPDATE";
    private static final String API_PEOPLE_MODLOG_TABLE = "PeopleDocuments";
    private static final String API_ORGANIZATION_MODLOG_TABLE = "OrganizationDocuments";
    private static final String REQUEST_WRAPPER_URI = "https://api.eclipse.org/foundation/hellosign";

    @Inject
    HellosignTemplatingConfiguration templateConfigs;

    @Inject
    DefaultHibernateDao dao;
    @Inject
    EclipsePersistenceDao eclipseDao;
    @Inject
    FilterService filters;

    @Inject
    CachingService cache;
    @Inject
    APIMiddleware middle;
    @Inject
    HellosignBindingService hellosign;
    @Inject
    ProfileService profile;
    @Inject
    MailerService mailer;
    @Inject
    ObjectMapper om;

    @RestClient
    FoundationDbAPI fdnApi;

    @Override
    public EfHellosignRequestData createNewSignatureRequest(HellosignSignatureRequest request) {
        // retrieve the template for creating the request
        DocumentSignatureTemplate template = getTemplateById(request.getTemplateName());
        if (template == null) {
            throw new BadRequestException(String.format("Could not find a template with ID '%s'", request.getTemplateName()));
        }
        // set up the request data to submit to Hellosign
        SignatureRequest signatureRequest = new SignatureRequest();
        signatureRequest.setMessage(request.getMessage());
        signatureRequest.setRequestTemplateId(request.getTemplateName());
        signatureRequest.setSubject(request.getSubject());
        signatureRequest.setTitle(template.templateName());

        // convert the signers from request format to persistence model
        List<DocumentSigner> convertedSigners = ConversionHelpers.convertIncomingSigners(request.getSigners());
        LOGGER.debug("Converted {} signers for current signature request of type {}", convertedSigners.size(), request.getTemplateName());

        // action the request and create the signature request
        SignatureRequestResponse r = hellosign.createSignatureRequest(template, signatureRequest, convertedSigners);
        // set the response back into the request record
        signatureRequest.setUpstreamResponse(serializeSignatureRequestResponse(r));
        // set the request ID into the associated records
        signatureRequest.setRequestId(r.getSignatureRequestId());
        signatureRequest.setCreated(DateTimeHelper.now());
        signatureRequest.setStatus(HellosignRequestStatus.ECLIPSE_API_HELLOSIGN_REQUEST_STATUS_UNFINISHED);
        convertedSigners.forEach(ds -> ds.setRequestId(r.getSignatureRequestId()));

        // generate the contextless request wrap to facilitate persistence
        RequestWrapper wrap = new FlatRequestWrapper(URI.create(REQUEST_WRAPPER_URI));
        // persist Hellosign request info to track the request
        dao.add(new RDBMSQuery<>(wrap, filters.get(SignatureRequest.class)), Arrays.asList(signatureRequest));
        dao.add(new RDBMSQuery<>(wrap, filters.get(DocumentSigner.class)), convertedSigners);

        // marshall the results into the API model for return
        return EfHellosignRequestData
                .builder()
                .setHellosignSignatureId(signatureRequest.getRequestId())
                .setMessage(signatureRequest.getMessage())
                .setSubject(signatureRequest.getSubject())
                .setTitle(signatureRequest.getTitle())
                .setTemplateName(signatureRequest.getRequestTemplateId())
                .setResponse(signatureRequest.getUpstreamResponse())
                .setCreated(signatureRequest.getCreated())
                .setRequestId(Long.toString(signatureRequest.getId()))
                .build();
    }

    @Override
    public void completeSignatureRequest(String upstreamId) {
        // prepare to fetch request information based on the request ID
        RequestWrapper wrap = new FlatRequestWrapper(URI.create(REQUEST_WRAPPER_URI));
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(SignatureAPIParameterNames.UPSTREAM_REQUEST_ID_RAW, upstreamId);

        // get the assorted required data points to complete the request
        List<SignatureRequest> requests = dao.get(new RDBMSQuery<>(wrap, filters.get(SignatureRequest.class), params));
        if (requests == null || requests.isEmpty()) {
            throw new BadRequestException("Could not find signature request with upstream ID of {}" + upstreamId);
        }
        SignatureRequest request = requests.get(0);

        // using the signature request record, get the template associated with the request
        DocumentSignatureTemplate template = getTemplateById(request.getRequestTemplateId());
        if (template == null) {
            throw new BadRequestException(String.format("Could not find a template with ID '%s'", request.getRequestTemplateId()));
        }

        // wrap in try-catch so that we can handle errors and rethrow
        try {
            // fetch the client signer for the current request
            DocumentSigner client = getClient(upstreamId, wrap, params);
            // get users to apply document changes to
            List<PeopleData> targetUsers = retrieveUsersForCall(client, template);

            // do document processing for the current request
            boolean doCLAUpdate = doDocumentUpdatesForRequest(upstreamId, template, client, targetUsers,
                    Arrays.stream(hellosign.getDocumentForRequest(upstreamId)).toList());

            // do potential updates to CLA permissions associated with current request
            handleCLAPermissions(wrap, targetUsers, doCLAUpdate);
        } catch (RuntimeException e) {
            // process the error by updating status and sending out notifications
            handleProcessingError(template, request);
            // rethrow to continue bubbling up and error out
            throw e;
        }

        // update the request to mark it as complete
        request.setStatus(HellosignRequestStatus.ECLIPSE_API_HELLOSIGN_REQUEST_STATUS_FINISHED);
        dao.add(new RDBMSQuery<>(wrap, filters.get(SignatureRequest.class)), Arrays.asList(request));
    }

    /**
     * Retrieves and revokes documents associated with the request for the passed users. Documents are revoked by setting the expiration
     * date to now.
     * 
     * @param template the template for the current document request
     * @param targetUsers the users to revoke documents for
     */
    private void handleRevokedPersonalDocuments(DocumentSignatureTemplate template, List<PeopleData> targetUsers) {
        // map the users to their usernames to reduce potential duplicate operations in middleware
        List<String> usernames = targetUsers.stream().map(PeopleData::personID).toList();
        middle
                .getAll(i -> fdnApi.getPeopleDocuments(i, template.revokedDocuments(), usernames))
                .stream()
                .map(doc -> PeopleDocumentDataBuilder.from(doc).withExpirationDate(new Date()))
                .forEach(doc -> {
                    // run the update, and then do the associated modlog
                    RestResponse<List<PeopleDocumentData>> updateResponse = fdnApi.updatePeopleDocument(doc.personID(), doc);
                    if (updateResponse.getStatus() == Response.Status.OK.getStatusCode()) {
                        fdnApi
                                .updateModlog(SysModLogDataBuilder
                                        .builder()
                                        .logAction(API_MODLOG_ACTION)
                                        .logTable(API_PEOPLE_MODLOG_TABLE)
                                        .modDateTime(DateTimeHelper.now())
                                        .personId(API_MODLOG_AUTHOR)
                                        .pk1(doc.personID())
                                        .pk2("")
                                        .build());
                    } else {
                        LOGGER.error("Error while revoking document {} for user {}", doc.documentID(), doc.personID());
                    }
                });
    }

    /**
     * For organization level document requests, handle revoking documents associated with the request. Will attempt to revoke all documents
     * set into the template, continuing even if there are errors as this is considered a non fatal error point.
     * 
     * @param template document signature template for current request
     * @param organizationId the organization to revoke documents from
     * @return true if all revoke operations were successful, false otherwise.
     */
    private boolean handleRevokedOrganizationalDocuments(DocumentSignatureTemplate template, Integer organizationId) {
        return middle
                .getAll(i -> fdnApi.getOrganizationDocuments(i, organizationId, template.revokedDocuments()))
                .stream()
                .map(od -> OrganizationDocumentDataBuilder.from(od).withExpirationDate(new Date()))
                .map(this::performStatusUpdateWithModlog)
                .allMatch(b -> b);
    }

    /**
     * Performs an update to an OrganizationDocument in fdndb-api, performing the associated ModLog operation on a successful update.
     * Returns true or false based on the success of the update operation.
     * 
     * @param odd The OrganizationDocumentData to update.
     * @return True if update success. False if not.
     */
    private boolean performStatusUpdateWithModlog(OrganizationDocumentData odd) {
        // run the update, and then do the associated modlog operation
        RestResponse<OrganizationDocumentData> updateResponse = fdnApi.updateOrganizationDocument(odd.organizationID(), odd);
        if (updateResponse.getStatus() == Response.Status.OK.getStatusCode()) {
            fdnApi
                    .updateModlog(SysModLogDataBuilder
                            .builder()
                            .logAction(API_MODLOG_ACTION)
                            .logTable(API_ORGANIZATION_MODLOG_TABLE)
                            .modDateTime(DateTimeHelper.now())
                            .personId(API_MODLOG_AUTHOR)
                            .pk1(Integer.toString(odd.organizationID()))
                            .pk2("")
                            .build());
            return true;
        } else {
            LOGGER.error("Error while revoking document {} for organization {}", odd.documentID(), odd.organizationID());
        }
        return false;
    }

    /**
     * Persist documents related to the request for a personal document template. The counterpart to this method is
     * handleOrganizationalDocumentPersist and should be used when the mcca flag is set to true.
     * 
     * @param pdfContents
     * @param client
     * @param individual the information for the signer client that initiated the request
     * @param template the template used for the request
     * @return whether the main document was successfully added. While related docs are also handled, their success shouldn't block
     * following steps
     */
    private boolean handlePersonalDocumentPersist(List<Byte> pdfContents, DocumentSigner client, PeopleData individual,
            DocumentSignatureTemplate template) {
        boolean mainDocSuccess = true;
        // create the base document used in other documents
        PeopleDocumentData baseDoc = PeopleDocumentDataBuilder
                .builder()
                .documentID(template.document().id())
                .effectiveDate(new Date())
                .personID(client.getUsername())
                .receivedDate(new Date())
                .version(template.document().version())
                .scannedDocumentBLOB(pdfContents)
                .scannedDocumentBytes(pdfContents.size())
                .scannedDocumentMime("application/pdf")
                .scannedDocumentFileName(String.format(template.fileNamePattern(), client.getUsername()))
                .build();

        String username = individual.personID();
        // persist the base document
        RestResponse<List<PeopleDocumentData>> baseDocResponse = fdnApi.updatePeopleDocument(individual.personID(), baseDoc);
        if (baseDocResponse.getStatus() == Response.Status.OK.getStatusCode()) {
            LOGGER.debug("Successfully inserted base document for '{}' of document '{}'", individual.personID(), baseDoc.documentID());
            fdnApi
                    .updateModlog(SysModLogDataBuilder
                            .builder()
                            .logAction(API_MODLOG_ACTION)
                            .logTable(API_PEOPLE_MODLOG_TABLE)
                            .modDateTime(DateTimeHelper.now())
                            .personId(API_MODLOG_AUTHOR)
                            .pk1(username)
                            .pk2(template.document().message().orElse(template.document().id()))
                            .build());
        } else {
            LOGGER.error("Could not insert main document, could not complete request");
            mainDocSuccess = false;
        }

        processRelatedPersonalDocuments(template, baseDoc, username);
        return mainDocSuccess;
    }

    /**
     * Update documents related to the primary document as defined in the template. This method will only update existing documents and will
     * not generate new documents.
     *
     * @param template signature request template to apply to the request
     * @param baseDoc the document that should be used as the base when generating document updates for related docs
     * @param username ID of the user to update documents for
     */
    private void processRelatedPersonalDocuments(DocumentSignatureTemplate template, PeopleDocumentData baseDoc, String username) {
        // prep the list of documents to fetch
        List<String> targetDocuments = template.relatedDocuments().stream().map(DocumentReference::id).toList();
        LOGGER.debug("Looking for following documents for user {}: {}", username, targetDocuments);
        List<PeopleDocumentData> relatedDocs = middle
                .getAll(i -> fdnApi.getPeopleDocumentsForUser(i, username, targetDocuments))
                .stream()
                .filter(pdd -> pdd.expirationDate() == null || pdd.expirationDate().after(new Date()))
                .toList();
        LOGGER
                .debug("Found existing documents {} for user {}", relatedDocs.stream().map(PeopleDocumentData::documentID).toList(),
                        username);
        // for each of the related documents, only update documents that already exist
        template
                .relatedDocuments()
                .stream()
                .filter(dr -> relatedDocs
                        .stream()
                        .filter(pdd -> pdd.expirationDate() == null || pdd.expirationDate().after(new Date()))
                        .anyMatch(pdd -> pdd.documentID().equals(dr.id())))
                .forEach(dr -> {
                    LOGGER.debug("Adding document {} for user {}", dr.id(), username);
                    // generate document, persist, and log success/failure
                    RestResponse<List<PeopleDocumentData>> relatedDocResponse = fdnApi
                            .updatePeopleDocument(username,
                                    PeopleDocumentDataBuilder.builder(baseDoc).documentID(dr.id()).version(dr.version()).build());
                    // check if returned doc was successfully created, create mod log based on success/failure
                    if (relatedDocResponse.getStatus() == Response.Status.OK.getStatusCode()) {
                        fdnApi
                                .updateModlog(SysModLogDataBuilder
                                        .builder()
                                        .logAction(API_MODLOG_ACTION)
                                        .logTable(API_PEOPLE_MODLOG_TABLE)
                                        .modDateTime(DateTimeHelper.now())
                                        .personId(API_MODLOG_AUTHOR)
                                        .pk1(username)
                                        .pk2(dr.message().orElse(dr.id()))
                                        .build());
                    } else {
                        LOGGER.error("Could not generate document {} for user {}", dr.id(), username);
                    }
                });
    }

    /**
     * Handle updating organization signature request document persisting. Takes the completed document body retrieved from Hellosign and
     * persists it to the the foundation DB.
     * 
     * @param pdfContents the PDF contents from Hellosign for the completed request
     * @param client the client signer for the corresponding document signature request
     * @param template document template used for the current request
     * @param organizationId the organization corresponding to the client signer
     * @return true if the documents were successfully updated, false otherwise
     */
    private boolean handleOrganizationalDocumentPersist(List<Byte> pdfContents, DocumentSigner client, DocumentSignatureTemplate template,
            Integer organizationId) {
        OrganizationDocumentData baseDoc = OrganizationDocumentDataBuilder
                .builder()
                .documentID(template.document().id())
                .effectiveDate(new Date())
                .organizationID(organizationId)
                .receivedDate(new Date())
                .version(template.document().version())
                .scannedDocumentBLOB(pdfContents)
                .scannedDocumentBytes(pdfContents.size())
                .scannedDocumentMime("application/pdf")
                .scannedDocumentFileName(String.format(template.fileNamePattern(), client.getName()))
                .build();
        // do the document update, retrieving the response to check call success
        RestResponse<OrganizationDocumentData> baseDocResposne = fdnApi.updateOrganizationDocument(organizationId, baseDoc);
        // if the call was successful, log the update action, otherwise log an error
        if (baseDocResposne.getStatus() == Response.Status.OK.getStatusCode()) {
            LOGGER.debug("Successfully inserted base document for '{}' of document '{}'", organizationId, baseDoc.documentID());
            fdnApi
                    .updateModlog(SysModLogDataBuilder
                            .builder()
                            .logAction(API_MODLOG_ACTION)
                            .logTable(API_PEOPLE_MODLOG_TABLE)
                            .modDateTime(DateTimeHelper.now())
                            .personId(API_MODLOG_AUTHOR)
                            .pk1(Integer.toString(organizationId))
                            .pk2(template.document().message().orElse(template.document().id()))
                            .build());
        } else {
            LOGGER.error("Could not insert main document, could not complete request");
            return false;
        }
        return true;
    }

    /**
     * Perform updates to documents for individuals or organizations associated with the current completed Hellosign signature request.
     * 
     * @param upstreamId Hellosign request ID for the current local request
     * @param template template associated with the signature request
     * @param client signature client for the current request, used to either lookup organizations or is the indivudal to update documents
     * for
     * @param targetUsers the users associated with the current request
     * @param doc the completed PDF contents as bytes from Hellosign
     * @return whether the document updates were all successful
     */
    private boolean doDocumentUpdatesForRequest(String upstreamId, DocumentSignatureTemplate template, DocumentSigner client,
            List<PeopleData> targetUsers, List<Byte> doc) {
        if (template.mcca()) {
            Optional<Integer> organizationIdOpt = getOrganizationIdForClient(client);
            if (organizationIdOpt.isEmpty()) {
                throw new BadRequestException("Could not find organization associated to current organization document signature request");
            }
            Integer organizationId = organizationIdOpt.get();
            // revoke documents for organization and individuals
            boolean successfullyRevokedOrgDocuments = handleRevokedOrganizationalDocuments(template, organizationId);
            handleRevokedPersonalDocuments(template, targetUsers);

            // persist the documents for the organization
            boolean successfullyInsertedOrganizationDocuments = handleOrganizationalDocumentPersist(doc, client, template, organizationId);
            // only do CLA updates if the organization documents were successfully revoked and inserted
            return successfullyInsertedOrganizationDocuments && successfullyRevokedOrgDocuments;
        } else {
            // retrieve documents to revoke, then trigger updates to mark them as revoked
            handleRevokedPersonalDocuments(template, targetUsers);

            // handle the documents for each of the affected users
            return targetUsers.stream().map(user -> {
                LOGGER.trace("Processing user '{}' for request '{}'", user.personID(), upstreamId);
                return handlePersonalDocumentPersist(doc, client, user, template);
            }).allMatch(b -> b);
        }
    }

    /**
     * Using the passed parameters, handle CLA permission request updates associated with completing a signature request.
     * 
     * @param wrap the current request wrapper for the completion request
     * @param targetUsers the users to update CLA records for
     * @param doCLAUpdate whether the CLA update should be performed
     */
    private void handleCLAPermissions(RequestWrapper wrap, List<PeopleData> targetUsers, boolean doCLAUpdate) {
        if (doCLAUpdate) {
            // for each user, create an account request to update the CLA perms
            eclipseDao.add(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class)), targetUsers.stream().map(user -> {
                LOGGER.info("Creating account request for user {} to update CLA permissions", user.personID());
                AccountRequestsCompositeId id = new AccountRequestsCompositeId();
                id.setEmail(user.email());
                id.setReqWhen(LocalDateTime.now());
                AccountRequests ar = new AccountRequests();
                ar.setId(id);
                ar.setFname(user.fname());
                ar.setLname(user.lname());
                ar.setPassword("eclipsecla");
                ar.setToken("CLA_SIGNED");
                return ar;
            }).toList());
        }
    }

    /**
     * Retrieve the client document signer for the current request.
     * 
     * @param upstreamId the upstream document signature request ID
     * @param wrap the current request wrapper for the request
     * @param params parameters for lookups related to the current request.
     * @return the client document signer for the current request
     * @throws BadRequestException when there is no client signer for the current request, which is required for any active request.
     */
    private DocumentSigner getClient(String upstreamId, RequestWrapper wrap, MultivaluedMap<String, String> params) {
        List<DocumentSigner> signers = dao.get(new RDBMSQuery<>(wrap, filters.get(DocumentSigner.class), params));
        if (signers == null || signers.isEmpty()) {
            throw new BadRequestException("Could not find signers with upstream ID: " + upstreamId);

        }
        Optional<DocumentSigner> clientSigner = signers.stream().filter(s -> "client".equalsIgnoreCase(s.getRole())).findFirst();
        if (clientSigner.isEmpty()) {
            throw new BadRequestException("No client signer detected for request with upstream ID " + upstreamId);
        }
        return clientSigner.get();
    }

    /**
     * Get people associated with the request. This can either be a single individual for personal record signatures, or all committers
     * associated with the same organization for organization-level document requests.
     * 
     * @param client the signature client to use as the base for requests.
     * @param template the document template for the current signature request
     * @return the list of users associated with the request. For personal requests, this will be a single entry. For organization level
     * document signatures, this will contain a list of registered committers.
     */
    private List<PeopleData> retrieveUsersForCall(DocumentSigner client, DocumentSignatureTemplate template) {
        List<PeopleData> targetUsers = new ArrayList<>();
        if (template.mcca()) {
            Optional<Integer> orgId = getOrganizationIdForClient(client);
            if (orgId.isEmpty()) {
                throw new IllegalStateException(String
                        .format("Could not find an organization associated with user '%s' for request '%s'", client.getName(),
                                client.getRequestId()));
            }

            // using the cached org ID, get all assoc. users
            targetUsers
                    .addAll(middle
                            .getAll(i -> fdnApi.getOrganizationContact(i, null, "CR", orgId.get()))
                            .stream()
                            .map(FullOrganizationContactData::person)
                            .toList());
        } else {
            targetUsers.add(getOrInsertFoundationUser(client.getUsername(), client.getMail()));
        }
        return targetUsers;
    }

    /**
     * Retrieve the cached organization ID for a signer in an organizational-level signature request.
     * 
     * @param docSigner the client for the request to use to retrieve the associated organization ID
     * @return the organization ID if it can be found, otherwise empty
     */
    private Optional<Integer> getOrganizationIdForClient(DocumentSigner docSigner) {
        return cache.get(docSigner.getName(), new MultivaluedHashMap<>(), Integer.class, () -> {
            List<FullOrganizationContactData> clientContactRecord = middle
                    .getAll(i -> fdnApi.getOrganizationContact(i, docSigner.getUsername(), "CR", null));
            if (clientContactRecord == null || clientContactRecord.isEmpty()) {
                throw new IllegalStateException(String
                        .format("Request is indicated as an organization-level document, but client signer %s is not part of an organization",
                                docSigner.getName()));
            }
            return clientContactRecord.get(0).organization().organizationID();
        }).data();
    }

    /**
     * Attempts to first retrieve a Foundation DB record for the user, creating it if missing. Information from the Accounts API will be
     * fetched to populate the missing fields for the table.
     * 
     * @param username the username to fetch
     * @param mail the associated email to fetch
     * @return the Foundation DB Person record
     */
    private PeopleData getOrInsertFoundationUser(String username, String mail) {
        // check FDNDB API for user existence
        PeopleData personalRecord = getFoundationPersonRecord(username);
        if (personalRecord == null) {
            // get accounts data for the given user to base the foundation record on
            profile.performUserSearch(new UserSearchParams(null, username, null));
            Optional<EfUser> uOpt = profile.performUserSearch(new UserSearchParams(null, username, null));
            if (uOpt.isEmpty()) {
                throw new NotFoundException(String.format("Cannot find Eclipse account for user %s to build Foundation record", username));
            }
            EfUser u = uOpt.get();

            // generate the new person record from the Eclipse account
            List<PeopleData> createdPeople = fdnApi
                    .updatePeople(PeopleDataBuilder
                            .builder()
                            .blog("")
                            .comments(
                                    "Created through the Document Signature API - " + DateTimeHelper.now().toString())
                            .email(mail)
                            .fax("")
                            .fname(u.firstName())
                            .issuesPending(false)
                            .latitude("")
                            .lname(u.lastName())
                            .longitude("")
                            .member(u.orgId() != null)
                            .memberPassword("")
                            .memberSince(null)
                            .mobile("")
                            .personID(username)
                            .phone("")
                            .provisioning("")
                            .scrmGuid("")
                            .type(u.orgId() != null ? "EM" : "XX")
                            .unixAcctCreated(false)
                            .build());
            if (createdPeople == null || createdPeople.isEmpty()) {
                throw new ServerErrorException("Error while creating people record for " + username,
                        Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
            } else {
                personalRecord = createdPeople.get(0);
            }
        }
        // return the first entry which should be the newly created person record
        return personalRecord;
    }

    /**
     * Retrieves a cached user entry from the foundationDB if it exists.
     * 
     * @param username the username to look up in the foundation DB
     * @return the user object if it exists, or null if missing
     */
    private PeopleData getFoundationPersonRecord(String username) {
        Optional<PeopleData> cachedRecord = cache
                .get(username, new MultivaluedHashMap<>(), PeopleData.class, () -> fdnApi.getPersonRecord(username))
                .data();
        if (cachedRecord.isEmpty()) {
            LOGGER.info("Unable to find user record for {}", username);
            return null;
        }
        return cachedRecord.get();
    }

    private void handleProcessingError(DocumentSignatureTemplate template, SignatureRequest request) {
        LOGGER.warn("Request with ID {} is in an error state, updating entry and sending notification", request.getId());
        // update the request status to the error state and save it
        request.setStatus(HellosignRequestStatus.ECLIPSE_API_HELLOSIGN_REQUEST_STATUS_ERROR);
        dao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create(REQUEST_WRAPPER_URI)), filters.get(SignatureRequest.class)),
                        Arrays.asList(request));

        // send the message to EMO that the request is in a bad state
        mailer
                .sendMailToEMO(BadStateNotificationParams
                        .builder()
                        .setDocumentType(template.comment())
                        .setHid(request.getId())
                        .setServerRoot("")
                        .setEntityType(template.mcca() ? "organization" : "account")
                        .build());
    }

    /**
     * Serializes the signature request response from Hellosign, converting it into JSON data to be stored along with the request record.
     * 
     * @param r the response from Hellosign for creating the signature request
     * @return the JSON data for the response, or null if it can't be converted
     */
    private String serializeSignatureRequestResponse(SignatureRequestResponse r) {
        try {
            return om.writeValueAsString(r);
        } catch (JsonProcessingException e) {
            LOGGER.error("Error processing request response for request {}", r.getSignatureRequestId());
            return null;
        }
    }

    /**
     * Retrieves templates by their ID, using both the current name as well as past aliases to support legacy entries.
     * 
     * @param id the template ID or alias being fetched
     * @return matching template if it exists, otherwise null
     */
    private DocumentSignatureTemplate getTemplateById(String id) {
        return templateConfigs
                .templates()
                .entrySet()
                .stream()
                .filter(e -> e.getKey().equals(id) || e.getValue().aliases().contains(id))
                .map(Entry::getValue)
                .findFirst()
                .orElse(null);
    }
}
