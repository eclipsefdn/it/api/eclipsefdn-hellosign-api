/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.services;

import java.util.List;

import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration.DocumentSignatureTemplate;
import org.eclipsefoundation.hellosign.dtos.docsign.DocumentSigner;
import org.eclipsefoundation.hellosign.dtos.docsign.SignatureRequest;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;

import com.dropbox.sign.model.EventCallbackRequest;
import com.dropbox.sign.model.SignatureRequestResponse;

/**
 * Interface for service, binding to Hellosign and manage the communications.
 * 
 * @author Martin Lowe
 */
public interface HellosignBindingService {

    /**
     * Retrieves the document file contents for the given Hellosign request.
     * 
     * @param requestId Hellosign request ID of request to retrieve document for
     * @return the byte array containing the file contents if available
     */
    Byte[] getDocumentForRequest(String requestId);
    
    /**
     * 
     * @param payload
     * @return
     */
    boolean validateIncomingRequest(EventCallbackRequest payload);

    /**
     * Process an incoming request from Hellosign, indicating an event within the system, changing the state of one of
     * the active requests.
     * 
     * @param payload the incoming payload from Hellosign
     * @throws FinalForbiddenException if the request is deemed illegitimate
     */
    void processIncomingDocumentEvent(EventCallbackRequest payload);

    /**
     * Using the provided template, request params, and signers, create a signature request within Hellosign.
     * 
     * @param template current template to apply to request
     * @param request the incoming request to generate signatures
     * @param signers the individuals involved with the signature request.
     * @return the completed signature request from Hellosign.
     */
    SignatureRequestResponse createSignatureRequest(DocumentSignatureTemplate template, SignatureRequest request,
            List<DocumentSigner> signers);
}
