/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.namespaces;

/**
 * Authorization scopes used by this API.
 */
public class DocumentSignatureAuthScopes {

    public static final String CREATE = "eclipsefdn_create_documents";
    
    /**
     * Empty constructor, as all members are public static and this class should not be initialized. 
     */
    private DocumentSignatureAuthScopes() {
    }
}
