/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.config;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;
import io.smallrye.config.WithName;

/**
 * Stores information about Document Signature templates for various workflows.
 * 
 * @author Martin Lowe
 *
 */
@ConfigMapping(prefix = "eclipse.hellosign")
public interface HellosignTemplatingConfiguration {

    /*
     * The templates available to the API for processing.
     */
    Map<String, DocumentSignatureTemplate> templates();

    boolean testMode();

    /*
     * Allows test mode to be set separately from incoming request validation for safer testing.
     */
    @WithDefault("true")
    boolean enableRequestValidation();

    @WithDefault("")
    String clientId();
    
    @WithDefault("")
    String applicationToken();

    /**
     * Contains the information required to action a request for signature, all fields are required.
     */
    interface DocumentSignatureTemplate {
        @JsonProperty
        String templateName();

        @JsonProperty
        String templateId();

        String fileNamePattern();

        @JsonProperty
        String comment();

        @JsonProperty
        DocumentReference document();

        @JsonProperty
        @WithDefault("false")
        boolean mcca();

        @JsonProperty
        @WithName("related-docs")
        @WithDefault("{}")
        List<DocumentReference> relatedDocuments();

        @JsonProperty
        @WithDefault("{}")
        List<String> revokedDocuments();
        
        /**
         * List of aliases in the system, used to map legacy template IDs to new formats.
         * 
         * @return list of aliases used by this template
         */
        @JsonProperty
        @WithDefault("{}")
        List<String> aliases();
    }

    /**
     * Information about a document, including the message to be included in logs and comments.
     */
    interface DocumentReference {
        @JsonProperty
        String id();

        @JsonProperty
        Double version();

        @JsonProperty
        Optional<String> message();
    }
}
