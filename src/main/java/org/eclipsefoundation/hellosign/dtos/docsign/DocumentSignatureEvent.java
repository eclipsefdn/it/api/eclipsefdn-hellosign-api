/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.dtos.docsign;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.hellosign.config.DateTimeAttributeConverter;
import org.eclipsefoundation.hellosign.namespaces.SignatureAPIParameterNames;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.utils.helper.DateTimeHelper;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Represents an event received by the document signature callback endpoint. Used to track signature process for
 * different documents.
 * 
 * @author Martin Lowe
 *
 */
@Entity
@Table(name = "hellosign_event")
public class DocumentSignatureEvent extends BareNode {
    public static final DtoTable TABLE = new DtoTable(DocumentSignatureEvent.class, "dse");

    @Id
    @Column(name = "eid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String eventType;
    @Column(name = "signature_request_id")
    private String requestId;
    @Column(name = "body", columnDefinition = "longblob")
    private String eventBody;
    @Convert(converter = DateTimeAttributeConverter.class)
    private ZonedDateTime created;

    /**
     * @return the id
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * @param eventType the eventType to set
     */
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    /**
     * @return the requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * @param requestId the requestId to set
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * @return the eventBody
     */
    public String getEventBody() {
        return eventBody;
    }

    /**
     * @param eventBody the eventBody to set
     */
    public void setEventBody(String eventBody) {
        this.eventBody = eventBody;
    }

    /**
     * @return the created
     */
    public ZonedDateTime getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(created, eventBody, eventType, id, requestId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DocumentSignatureEvent other = (DocumentSignatureEvent) obj;
        return DateTimeHelper.looseCompare(created, other.created, Optional.of(ChronoUnit.SECONDS)) && Objects.equals(eventBody, other.eventBody)
                && Objects.equals(eventType, other.eventType) && Objects.equals(id, other.id)
                && Objects.equals(requestId, other.requestId);
    }

    @Singleton
    public static class DocumentSignerFilter implements DtoFilter<DocumentSignatureEvent> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);
            
            // the upstream request ID
            String requestId = params.getFirst(SignatureAPIParameterNames.UPSTREAM_REQUEST_ID_RAW);
            if (StringUtils.isNotBlank(requestId)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".requestId = ?",
                                new Object[] { requestId }));
            }

            // Entity ID
            String id = params.getFirst(DefaultUrlParameterNames.ID_PARAMETER_NAME);
            if (StringUtils.isNumeric(id)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?",
                                new Object[] { Long.parseLong(id) }));
            }

            // the event type recorded
            String eventType = params.getFirst(SignatureAPIParameterNames.EVENT_TYPE_NAME_RAW);
            if (StringUtils.isNotBlank(eventType)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".eventType = ?",
                                new Object[] { eventType }));
            }

            return statement;
        }

        @Override
        public Class<DocumentSignatureEvent> getType() {
            return DocumentSignatureEvent.class;
        }
    }
}
