/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_BadStateNotificationParams.Builder.class)
public abstract class BadStateNotificationParams {

    public abstract String getEntityType();

    public abstract String getDocumentType();

    public abstract Long getHid();

    public abstract String getServerRoot();

    public static Builder builder() {
        return new AutoValue_BadStateNotificationParams.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setEntityType(String entityType);

        public abstract Builder setDocumentType(String documentType);

        public abstract Builder setHid(Long hid);

        public abstract Builder setServerRoot(String serverRoot);

        public abstract BadStateNotificationParams build();
    }
}
