/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.model;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import org.eclipsefoundation.hellosign.namespaces.HellosignRequestStatus;

/**
 * Basic converter to simplify handling statuses of Hellosign requests.
 * 
 * @author Martin Lowe
 *
 */
@Converter
public class HellosignRequestStatusConverter implements AttributeConverter<HellosignRequestStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(HellosignRequestStatus attribute) {
        return attribute.getCode();
    }

    @Override
    public HellosignRequestStatus convertToEntityAttribute(Integer dbData) {
        return HellosignRequestStatus.getByCode(dbData);
    }

}
