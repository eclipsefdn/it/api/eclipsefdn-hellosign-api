/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.model;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

import jakarta.validation.constraints.NotEmpty;

/**
 * Incoming request model for creating new Hellosign signature requests.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_HellosignSignatureRequest.Builder.class)
public abstract class HellosignSignatureRequest {

    public abstract List<Signer> getSigners();

    public abstract String getTemplateName();

    public abstract String getSubject();

    public abstract String getMessage();

    public static Builder builder() {
        return new AutoValue_HellosignSignatureRequest.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        @NotEmpty
        public abstract Builder setSigners(List<Signer> signers);

        public abstract Builder setTemplateName(String templatename);

        public abstract Builder setSubject(String subject);

        public abstract Builder setMessage(String message);

        public abstract HellosignSignatureRequest build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_HellosignSignatureRequest_Signer.Builder.class)
    public abstract static class Signer {
        public abstract String getRole();

        public abstract String getFullname();

        public abstract String getMail();

        public abstract String getName();

        public static Builder builder() {
            return new AutoValue_HellosignSignatureRequest_Signer.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setRole(String role);

            public abstract Builder setFullname(String fullname);

            public abstract Builder setMail(String mail);

            public abstract Builder setName(String name);

            public abstract Signer build();
        }
    }
}
