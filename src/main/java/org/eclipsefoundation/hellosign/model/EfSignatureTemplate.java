/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.hellosign.model;

import java.util.List;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Signature template information model.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_EfSignatureTemplate.Builder.class)
public abstract class EfSignatureTemplate {

    public abstract String getName();

    public abstract String getTemplateId();

    public abstract String getTemplateName();

    public abstract String getComment();

    public abstract TemplateDocument getDocument();

    public abstract boolean getMcca();

    public abstract List<TemplateDocument> getRelatedDocuments();

    public abstract List<String> getRevokedDocuments();

    public abstract List<String> getAliases();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_EfSignatureTemplate.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setName(String name);

        public abstract Builder setTemplateId(String templateId);

        public abstract Builder setTemplateName(String templateName);

        public abstract Builder setComment(String comment);

        public abstract Builder setDocument(TemplateDocument document);

        public abstract Builder setMcca(boolean mcca);

        public abstract Builder setRelatedDocuments(List<TemplateDocument> relatedDocuments);

        public abstract Builder setRevokedDocuments(List<String> revokedDocuments);

        public abstract Builder setAliases(List<String> aliases);

        public abstract EfSignatureTemplate build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_EfSignatureTemplate_TemplateDocument.Builder.class)
    public abstract static class TemplateDocument {

        public abstract String getId();

        @Nullable
        public abstract Double getVersion();

        @Nullable
        public abstract String getMessage();

        public static Builder builder() {
            return new AutoValue_EfSignatureTemplate_TemplateDocument.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setId(String id);

            public abstract Builder setVersion(@Nullable Double version);

            public abstract Builder setMessage(@Nullable String message);

            public abstract TemplateDocument build();
        }
    }
}
