/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.apis;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysModLogData;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import io.quarkus.security.Authenticated;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * Interface for interacting with FoundationDB API.
 * 
 * @author Martin Lowe
 *
 */
@OidcClientFilter
@Path("")
@Authenticated
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface FoundationDbAPI {

    @GET
    @Path("organizations/contacts/full")
    RestResponse<List<FullOrganizationContactData>> getOrganizationContact(@BeanParam BaseAPIParameters params, @QueryParam("personID") String username,
            @QueryParam("relation") String relation, @QueryParam("organization_id") Integer organizationId);

    @GET
    @Path("organizations/{organization_id}/documents")
    RestResponse<List<OrganizationDocumentData>> getOrganizationDocuments(@BeanParam BaseAPIParameters params,
            @PathParam("organization_id") Integer organizationId, @QueryParam("doc_ids") List<String> documentIds);

    @GET
    @Path("people/{user}")
    PeopleData getPersonRecord(@PathParam("user") String username);

    @GET
    @Path("people/documents")
    RestResponse<List<PeopleDocumentData>> getPeopleDocuments(@BeanParam BaseAPIParameters params, @QueryParam("document_ids") List<String> docIds,
            @QueryParam("people_ids") List<String> peopleIds);

    @GET
    @Path("people/{user}/documents")
    RestResponse<List<PeopleDocumentData>> getPeopleDocumentsForUser(@BeanParam BaseAPIParameters params, @PathParam("user") String username,
            @QueryParam("document_ids") List<String> docIds);

    @PUT
    @Path("people/{user}/documents")
    RestResponse<List<PeopleDocumentData>> updatePeopleDocument(@PathParam("user") String username, PeopleDocumentData payload);

    @PUT
    @RolesAllowed("fdb_write_organizations_documents")
    @Path("organizations/{organization_id}/documents")
    RestResponse<OrganizationDocumentData> updateOrganizationDocument(@PathParam("organization_id") Integer organizationId,
            OrganizationDocumentData payload);

    @PUT
    @RolesAllowed("fdb_write_people")
    @Path("people")
    List<PeopleData> updatePeople(PeopleData payload);

    @PUT
    @RolesAllowed("fdb_write_sys_modlog")
    @Path("sys/mod_logs")
    List<SysModLogData> updateModlog(SysModLogData payload);
}
