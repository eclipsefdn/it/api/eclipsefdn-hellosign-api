/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.resources;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration;
import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration.DocumentReference;
import org.eclipsefoundation.hellosign.config.HellosignTemplatingConfiguration.DocumentSignatureTemplate;
import org.eclipsefoundation.hellosign.dtos.docsign.DocumentSignatureEvent;
import org.eclipsefoundation.hellosign.dtos.docsign.SignatureRequest;
import org.eclipsefoundation.hellosign.model.EfHellosignRequestData;
import org.eclipsefoundation.hellosign.model.EfSignatureTemplate;
import org.eclipsefoundation.hellosign.model.EfSignatureTemplate.TemplateDocument;
import org.eclipsefoundation.hellosign.model.HellosignSignatureRequest;
import org.eclipsefoundation.hellosign.model.HellosignSignatureRequest.Signer;
import org.eclipsefoundation.hellosign.model.mappers.DocumentSignatureEventMapper;
import org.eclipsefoundation.hellosign.model.mappers.SignatureRequestMapper;
import org.eclipsefoundation.hellosign.namespaces.DocumentSignatureAuthScopes;
import org.eclipsefoundation.hellosign.namespaces.SignatureAPIParameterNames;
import org.eclipsefoundation.hellosign.services.DocumentSignatureService;
import org.eclipsefoundation.hellosign.services.HellosignBindingService;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.Authenticated;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

/**
 * Resource containing CRUD endpoints for interacting with Hellosign signature requests made by the system.
 * 
 * @author Martin Lowe
 *
 */
@Path("")
@Authenticated
@Produces(MediaType.APPLICATION_JSON)
public class HellosignResources {
    private static final Logger LOGGER = LoggerFactory.getLogger(HellosignResources.class);

    @Inject
    HellosignTemplatingConfiguration templateConfigs;

    @Inject
    FilterService filters;
    @Inject
    DefaultHibernateDao dao;
    @Inject
    CachingService cache;
    @Inject
    HellosignBindingService hellosign;
    @Inject
    DocumentSignatureService signatureService;

    @Inject
    SignatureRequestMapper requestMapper;
    @Inject
    DocumentSignatureEventMapper eventMapper;

    @Inject
    RequestWrapper wrapper;

    @GET
    public List<EfHellosignRequestData> getAllHellosignRequests() {
        // add pagination mappings to handle caching more smoothly
        MultivaluedMap<String, String> paginationParams = new MultivaluedHashMap<>();
        paginationParams
                .add(DefaultUrlParameterNames.PAGE_PARAMETER_NAME,
                        wrapper.getFirstParam(DefaultUrlParameterNames.PAGE).orElseGet(() -> "1"));
        paginationParams
                .add(DefaultUrlParameterNames.PAGESIZE_PARAMETER_NAME,
                        wrapper.getFirstParam(DefaultUrlParameterNames.PAGESIZE).orElseGet(() -> "25"));
        // get all results from the cache
        Optional<List<SignatureRequest>> allResults = cache
                .get("all", paginationParams, SignatureRequest.class,
                        () -> dao.get(new RDBMSQuery<SignatureRequest>(wrapper, filters.get(SignatureRequest.class))))
                .data();
        if (allResults.isEmpty()) {
            LOGGER.warn("Error while fetching all request results, returning empty list");
            return Collections.emptyList();
        }
        // convert the list of results to the JSON model and return
        return allResults.get().stream().map(requestMapper::toModel).toList();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed(DocumentSignatureAuthScopes.CREATE)
    public List<EfHellosignRequestData> createNewHellosignRequests(HellosignSignatureRequest request) {
        if (request.getSigners().isEmpty()) {
            throw new BadRequestException("Signers are required to create a signature request");
        }
        // get current signature requests for user
        List<SignatureRequest> currentRequestsForUser = dao
                .get(new RDBMSQuery<SignatureRequest>(wrapper, filters.get(SignatureRequest.class),
                        buildDuplicateRequestParams(request)));
        // if there is an inprocess request, don't create another one
        if (!currentRequestsForUser.isEmpty()) {
            throw new BadRequestException("There is already an active request for targeted client user");
        }

        return Arrays.asList(signatureService.createNewSignatureRequest(request));
    }

    @GET
    @Path("{id}")
    public EfHellosignRequestData getHellosignRequestById(@PathParam("id") Long id) {
        return cache
                .get(Long.toString(id), new MultivaluedHashMap<>(), EfHellosignRequestData.class,
                        () -> getAugmentedRequestData(id))
                .data()
                .orElseThrow(() -> new NotFoundException("Could not find a signature request with the given ID"));
    }

    @GET
    @Path("{id}/document")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getHellosignRequestDocumentById(@PathParam("id") Long id) {
        // add the ID to a param map, and pass to the RDBMS query to fetch single entry if it exists
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID_PARAMETER_NAME, Long.toString(id));
        SignatureRequest currentResult = dao
                .get(new RDBMSQuery<SignatureRequest>(wrapper, filters.get(SignatureRequest.class), params))
                .stream()
                .limit(1)
                .findFirst()
                .orElseThrow(() -> new NotFoundException("Could not find a signature request with the given ID"));

        // get the actual document contents from hellosign
        Byte[] docContents = hellosign.getDocumentForRequest(currentResult.getRequestId());

        // return the temp document as a file stream to the user
        return Response
                .ok(new ByteArrayInputStream(ArrayUtils.toPrimitive(docContents)))
                .header("content-disposition", String.format("attachment; filename=\"%s-current-signature.pdf\"", id))
                .build();
    }

    @GET
    @Path("templates")
    public List<EfSignatureTemplate> getAvailableTemplates() {
        return templateConfigs
                .templates()
                .entrySet()
                .stream()
                .map(e -> convertToSignatureTemplateModel(e.getKey(), e.getValue()))
                .toList();
    }

    /**
     * Build and retrieve the request data for the given Hellosign signature request, augmenting the return with the
     * events associated with the request if any exist.
     * 
     * @param id the internal ID for the signature request to retrieve.
     * @return the augmented request response if it exists, or null
     */
    private EfHellosignRequestData getAugmentedRequestData(Long id) {
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID_PARAMETER_NAME, Long.toString(id));

        return dao
                .get(new RDBMSQuery<SignatureRequest>(wrapper, filters.get(SignatureRequest.class), params))
                .stream()
                .limit(1)
                .map(r -> {
                    // map to the response model
                    EfHellosignRequestData data = requestMapper.toModel(r);
                    // set up params to fetch events related to the found request
                    MultivaluedMap<String, String> p = new MultivaluedHashMap<>();
                    p.add(SignatureAPIParameterNames.UPSTREAM_REQUEST_ID_RAW, r.getRequestId());

                    // retrieve associated events and set them into the data model
                    return data
                            .toBuilder()
                            .setEvents(dao
                                    .get(new RDBMSQuery<DocumentSignatureEvent>(wrapper,
                                            filters.get(DocumentSignatureEvent.class), p))
                                    .stream()
                                    .map(eventMapper::toModel)
                                    .toList())
                            .build();
                })
                .findFirst()
                .orElse(null);
    }

    private EfSignatureTemplate convertToSignatureTemplateModel(String name, DocumentSignatureTemplate templateConfig) {
        return EfSignatureTemplate
                .builder()
                .setName(name)
                .setTemplateId(templateConfig.templateId())
                .setTemplateName(templateConfig.templateName())
                .setComment(templateConfig.comment())
                .setDocument(convertDocumentToModel(templateConfig.document()))
                .setRelatedDocuments(templateConfig
                        .relatedDocuments()
                        .stream()
                        .map(this::convertDocumentToModel)
                        .toList())
                .setRevokedDocuments(templateConfig.revokedDocuments())
                .setMcca(templateConfig.mcca())
                .setAliases(templateConfig.aliases())
                .build();
    }

    private TemplateDocument convertDocumentToModel(DocumentReference doc) {
        return TemplateDocument
                .builder()
                .setId(doc.id())
                .setVersion(doc.version())
                .setMessage(doc.message().orElse(null))
                .build();
    }

    private MultivaluedMap<String, String> buildDuplicateRequestParams(HellosignSignatureRequest request) {
        // check that we have a client signer for the request
        Optional<Signer> client = request
                .getSigners()
                .stream()
                .filter(s -> "client".equalsIgnoreCase(s.getRole()))
                .findFirst();
        if (client.isEmpty()) {
            throw new BadRequestException("Client signer required for request");
        }
        // track the needed params to look for duplicate requests
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(SignatureAPIParameterNames.EMAIL_NAME_RAW, client.get().getMail());
        params.add(SignatureAPIParameterNames.ROLE_NAME_RAW, client.get().getRole());
        params.add(SignatureAPIParameterNames.TEMPLATE_NAME_RAW, request.getTemplateName());
        return params;
    }
}
