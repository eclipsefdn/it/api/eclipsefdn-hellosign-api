/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.resources;

import org.eclipsefoundation.hellosign.services.DocumentSignatureService;
import org.eclipsefoundation.hellosign.services.HellosignBindingService;
import org.jboss.resteasy.reactive.PartType;
import org.jboss.resteasy.reactive.RestForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dropbox.sign.model.EventCallbackRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Resource for handling external callbacks related to Hellosign signature requests.
 * 
 * @author Martin Lowe
 *
 */
@Path("callback")
@Consumes(MediaType.MULTIPART_FORM_DATA)
@Produces(MediaType.APPLICATION_JSON)
public class ExternalCallbacksResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalCallbacksResource.class);

    @Inject
    HellosignBindingService hellosign;
    @Inject
    DocumentSignatureService signatureService;

    @Inject
    ObjectMapper mapper;

    @POST
    public Response hellosignRequestEventsCallback(@RestForm("json") @PartType(MediaType.TEXT_PLAIN) String json)
            throws JsonProcessingException {
        // required, as the data for callbacks is posted as a form with JSON set as a string in the data
        EventCallbackRequest payload = mapper.readValue(json, EventCallbackRequest.class);
        // check that the incoming request is valid and from Hellosign
        hellosign.validateIncomingRequest(payload);

        // handle the different event types (we only care about signature request events)
        switch (payload.getEvent().getEventType()) {
            case SIGNATURE_REQUEST_ALL_SIGNED:
                LOGGER
                        .info("Recieved completion event for request {}, beginning processing",
                                payload.getSignatureRequest().getSignatureRequestId());
                signatureService.completeSignatureRequest(payload.getSignatureRequest().getSignatureRequestId());
                break;
            case CALLBACK_TEST:
                LOGGER.info("Recieved a test ping from Hellosign. Ping!");
                break;
            case ACCOUNT_CONFIRMED:
            case FILE_ERROR:
            case SIGN_URL_INVALID:
            case TEMPLATE_CREATED:
            case TEMPLATE_ERROR:
            case UNKNOWN_ERROR:
                LOGGER
                        .debug("Recieved callback for unsupported event type {}, which is not currently handled by API, dropping without processing",
                                payload.getEvent().getEventType());
                break;
            default:
                hellosign.processIncomingDocumentEvent(payload);
                break;
        }
        return Response.ok("Hello API Event Received").build();
    }
}
