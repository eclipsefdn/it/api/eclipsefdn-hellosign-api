/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.services.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.hellosign.model.BadStateNotificationParams;
import org.eclipsefoundation.hellosign.services.MailerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.mailer.MockMailbox;
import io.quarkus.test.junit.QuarkusTest;
import io.vertx.ext.mail.MailMessage;
import jakarta.inject.Inject;

/**
 * @author martin
 *
 */
@QuarkusTest
class DefaultMailerServiceTest {

    @ConfigProperty(name = "eclipse.mailer.hellosign.notification-address")
    String address;

    @Inject
    MockMailbox mailbox;

    @Inject
    MailerService mailer;

    @Test
    void sendMailToEMO_success_oneMessageSent() {
        String docType = "Test Document";
        int totalSentPreTest = mailbox.getTotalMessagesSent();
        // capture count of mail before test to get accurate count
        // list can be null, so fallback is needed
        List<MailMessage> sentMailPreTest = Optional.ofNullable(mailbox.getMailMessagesSentTo(address)).orElse(Collections.emptyList());

        mailer
                .sendMailToEMO(BadStateNotificationParams
                        .builder()
                        .setDocumentType(docType)
                        .setEntityType("")
                        .setHid(1L)
                        .setServerRoot("organization")
                        .build());

        List<MailMessage> sentMail = Optional.ofNullable(mailbox.getMailMessagesSentTo(address)).orElse(Collections.emptyList());
        Assertions
                .assertEquals(1, mailbox.getTotalMessagesSent() - totalSentPreTest,
                        "Expected 1 message to be sent for mailer service call");
        Assertions.assertEquals(1, sentMail.size() - sentMailPreTest.size(), "Expected message to be sent to the configured address");
    }

    @Test
    void sendMailToEMO_success_containsTextMail() {
        String docType = "Test Document";
        mailer
                .sendMailToEMO(BadStateNotificationParams
                        .builder()
                        .setDocumentType(docType)
                        .setEntityType("")
                        .setHid(1L)
                        .setServerRoot("organization")
                        .build());
        List<MailMessage> sentMail = mailbox.getMailMessagesSentTo(address);
        Assertions.assertFalse(sentMail.isEmpty(), "At least 1 message should have been sent by call");
        Assertions.assertNotNull(sentMail.get(0).getText(), "EMO message should have a text version available");
    }

    @Test
    void sendMailToEMO_success_subjectContainsDocType() {
        String docType = "Test Document";
        mailer
                .sendMailToEMO(BadStateNotificationParams
                        .builder()
                        .setDocumentType(docType)
                        .setEntityType("")
                        .setHid(1L)
                        .setServerRoot("organization")
                        .build());
        List<MailMessage> sentMail = mailbox.getMailMessagesSentTo(address);
        Assertions.assertFalse(sentMail.isEmpty(), "At least 1 message should have been sent by call");
        Assertions.assertTrue(sentMail.get(0).getSubject().contains(docType), "Mail subject should contain document type affected");
    }
}
