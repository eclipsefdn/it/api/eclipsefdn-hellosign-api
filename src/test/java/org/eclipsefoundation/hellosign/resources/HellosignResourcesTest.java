/**
 *
 * Copyright (c) 2023, 2024 Eclipse Foundation
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.resources;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.eclipsefoundation.hellosign.model.HellosignSignatureRequest;
import org.eclipsefoundation.hellosign.model.HellosignSignatureRequest.Signer;
import org.eclipsefoundation.hellosign.namespaces.DocumentSignatureAuthScopes;
import org.eclipsefoundation.hellosign.test.namespaces.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.dropbox.sign.ApiException;
import com.dropbox.sign.api.SignatureRequestApi;
import com.dropbox.sign.model.SignatureRequestGetResponse;
import com.dropbox.sign.model.SignatureRequestResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.InjectMock;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.quarkus.test.security.TestSecurity;
import io.restassured.http.ContentType;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * Tests for the Hellosign CRUD resources.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
@QuarkusTestResource(OidcWiremockTestResource.class)
class HellosignResourcesTest {
    public static final String API_BASE_URL = "/";
    public static final String REQUEST_BASE_URL = API_BASE_URL + "{id}";
    public static final String REQUEST_DOCUMENT_BASE_URL = REQUEST_BASE_URL + "/document";

    public static final String TEST_TEMPLATE_NAME = "ica";

    @InjectMock
    SignatureRequestApi reqApi;

    @Inject
    ObjectMapper mapper;

    /*
     * Get all requests
     */
    public static final EndpointTestCase GET_ALL_SUCCESS = TestCaseHelper
            .buildSuccessCase(API_BASE_URL, new String[] {}, SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH);
    public static final EndpointTestCase GET_ALL_AUTH_REQUIRED = TestCaseHelper
            .prepareTestCase(API_BASE_URL, new String[] {}, SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH)
            .setStatusCode(Status.UNAUTHORIZED.getStatusCode())
            .build();

    /*
     * Create new request
     */
    public static final EndpointTestCase CREATE_NEW_REQUEST_SUCCESS = TestCaseHelper
            .buildSuccessCase(API_BASE_URL, new String[] {}, SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH);
    public static final EndpointTestCase CREATE_NEW_REQUEST_AUTH_REQUIRED = TestCaseHelper
            .prepareTestCase(API_BASE_URL, new String[] {}, SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH)
            .setStatusCode(Status.UNAUTHORIZED.getStatusCode())
            .build();
    public static final EndpointTestCase CREATE_NEW_REQUEST_WRONG_SCOPE = TestCaseHelper
            .prepareTestCase(API_BASE_URL, new String[] {}, SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH)
            .setStatusCode(Status.FORBIDDEN.getStatusCode())
            .build();
    public static final EndpointTestCase CREATE_NEW_REQUEST_INVALID_REQUEST = TestCaseHelper
            .buildBadRequestCase(API_BASE_URL, new String[] {}, SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH);
    public static final EndpointTestCase CREATE_NEW_REQUEST_NOT_ACCEPTED = TestCaseHelper
            .prepareTestCase(API_BASE_URL, new String[] {}, SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH)
            .setStatusCode(Response.Status.BAD_REQUEST.getStatusCode())
            .build();

    /*
     * Get single request
     */
    public static final EndpointTestCase GET_SINGLE_SUCCESS = TestCaseHelper
            .buildSuccessCase(REQUEST_BASE_URL, new String[] { "1" }, SchemaNamespaceHelper.REQUEST_SCHEMA_PATH);
    public static final EndpointTestCase GET_SINGLE_AUTH_REQUIRED = TestCaseHelper
            .prepareTestCase(REQUEST_BASE_URL, new String[] { "1" }, SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH)
            .setStatusCode(Status.UNAUTHORIZED.getStatusCode())
            .build();
    public static final EndpointTestCase GET_SINGLE_REQUEST_NOT_FOUND = TestCaseHelper
            .buildNotFoundCase(REQUEST_BASE_URL, new String[] { "999" }, SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH);
    public static final EndpointTestCase GET_SINGLE_REQUEST_INVALID_ID = TestCaseHelper
            .buildNotFoundCase(REQUEST_BASE_URL, new String[] { "sample" }, SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH);
    /*
     * Get single request document
     */
    public static final EndpointTestCase GET_DOCUMENT_SUCCESS = TestCaseHelper
            .prepareTestCase(REQUEST_DOCUMENT_BASE_URL, new String[] { "1" }, null)
            .setRequestContentType(ContentType.BINARY)
            .setResponseContentType(ContentType.BINARY)
            .build();
    public static final EndpointTestCase GET_DOCUMENT_AUTH_REQUIRED = TestCaseHelper
            .prepareTestCase(REQUEST_DOCUMENT_BASE_URL, new String[] { "1" },
                    SchemaNamespaceHelper.REQUESTS_SCHEMA_PATH)
            .setRequestContentType(ContentType.BINARY)
            .setResponseContentType(ContentType.BINARY)
            .setStatusCode(Status.UNAUTHORIZED.getStatusCode())
            .build();
    public static final EndpointTestCase GET_DOCUMENT_NOT_FOUND = TestCaseHelper
            .prepareTestCase(REQUEST_DOCUMENT_BASE_URL, new String[] { "999" }, null)
            .setStatusCode(Status.NOT_FOUND.getStatusCode())
            .setRequestContentType(ContentType.BINARY)
            .setResponseContentType(ContentType.BINARY)
            .build();
    public static final EndpointTestCase GET_DOCUMENT_INVALID_ID = TestCaseHelper
            .prepareTestCase(REQUEST_DOCUMENT_BASE_URL, new String[] { "sample" }, null)
            .setStatusCode(Status.NOT_FOUND.getStatusCode())
            .setRequestContentType(ContentType.BINARY)
            .setResponseContentType(ContentType.BINARY)
            .build();

    @BeforeEach
    public void setupHellosignTestState() throws ApiException, IOException {
        File temp = File.createTempFile("test", ".pdf");

        SignatureRequestGetResponse mockedResponse = new SignatureRequestGetResponse();
        mockedResponse.signatureRequest(new SignatureRequestResponse().testMode(true).signatureRequestId("sample"));
        Mockito.when(reqApi.signatureRequestSendWithTemplate(Mockito.any())).thenReturn(mockedResponse);
        Mockito.when(reqApi.signatureRequestFiles(Mockito.anyString(), Mockito.anyString())).thenReturn(temp);
    }

    /*
     * Tests: GET /foundation/hellosign
     */
    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "test-role")
    void getAllHellosignRequests_success() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "test-role")
    void getAllHellosignRequests_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "test-role")
    void getAllHellosignRequests_success_matchingSpec() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getAllHellosignRequests_failure_authRequired() {
        EndpointTestBuilder.from(GET_ALL_AUTH_REQUIRED).run();
    }

    /*
     * Tests: POST /foundation/hellosign
     */
    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = DocumentSignatureAuthScopes.CREATE)
    void createNewHellosignRequests_success() {
        EndpointTestBuilder
                .from(CREATE_NEW_REQUEST_SUCCESS)
                .doPost(convertToJsonString(createSampleRequest(Arrays.asList("sample_user"), TEST_TEMPLATE_NAME)))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = DocumentSignatureAuthScopes.CREATE)
    void createNewHellosignRequests_success_validResponseFormat() {
        EndpointTestBuilder
                .from(CREATE_NEW_REQUEST_SUCCESS)
                .doPost(convertToJsonString(createSampleRequest(Arrays.asList("sample_user1"), TEST_TEMPLATE_NAME)))
                .andCheckFormat()
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = DocumentSignatureAuthScopes.CREATE)
    void createNewHellosignRequests_success_matchingSpec() {
        EndpointTestBuilder
                .from(CREATE_NEW_REQUEST_SUCCESS)
                .doPost(convertToJsonString(createSampleRequest(Arrays.asList("sample_user2"), TEST_TEMPLATE_NAME)))
                .andCheckSchema()
                .run();
    }

    @Test
    void createNewHellosignRequests_failure_authRequired() {
        EndpointTestBuilder
                .from(CREATE_NEW_REQUEST_AUTH_REQUIRED)
                .doPost(convertToJsonString(createSampleRequest(Arrays.asList("sample_user"), TEST_TEMPLATE_NAME)))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "some_other_scope")
    void createNewHellosignRequests_failure_creationScopeRequired() {
        EndpointTestBuilder
                .from(CREATE_NEW_REQUEST_WRONG_SCOPE)
                .doPost(convertToJsonString(createSampleRequest(Arrays.asList("sample_user"), TEST_TEMPLATE_NAME)))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = DocumentSignatureAuthScopes.CREATE)
    void createNewHellosignRequests_failure_requiresSigners() {
        // cannot even compile the signers without values, so hardcoded JSON needs to be used here
        EndpointTestBuilder
                .from(CREATE_NEW_REQUEST_INVALID_REQUEST)
                .doPost("{\"signers\": [], \"template_id\": \"required\", \"subject\": \"optional\", \"message\": \"optional\",\"template_name\":\"ica\"}")
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = DocumentSignatureAuthScopes.CREATE)
    void createNewHellosignRequests_failure_testModeExternalSigners() {
        // test mode (which is enabled in testing) does not accept external email addresses
        EndpointTestBuilder
                .from(CREATE_NEW_REQUEST_NOT_ACCEPTED)
                .doPost(convertToJsonString(HellosignSignatureRequest
                        .builder()
                        .setMessage("Sample message")
                        .setSubject("sample")
                        .setSigners(Arrays
                                .asList(Signer
                                        .builder()
                                        .setFullname("Test test")
                                        .setName("sample_user4")
                                        .setRole("Client")
                                        .setMail("sample@test.org")
                                        .build()))
                        .setTemplateName(TEST_TEMPLATE_NAME)
                        .build()))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = DocumentSignatureAuthScopes.CREATE)
    void createNewHellosignRequests_failure_invalidTemplateName() {
        // cannot even compile the signers without values, so hardcoded JSON needs to be used here
        EndpointTestBuilder
                .from(CREATE_NEW_REQUEST_INVALID_REQUEST)
                .doPost(convertToJsonString(createSampleRequest(Arrays.asList("sample_user"), "invalid-template")))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = DocumentSignatureAuthScopes.CREATE)
    void createNewHellosignRequests_failure_duplicateRequest() {
        // cannot even compile the signers without values, so hardcoded JSON needs to be used here
        EndpointTestBuilder
                .from(CREATE_NEW_REQUEST_SUCCESS)
                .doPost(convertToJsonString(createSampleRequest(Arrays.asList("sample_user3"), TEST_TEMPLATE_NAME)))
                .run();
        EndpointTestBuilder
                .from(CREATE_NEW_REQUEST_INVALID_REQUEST)
                .doPost(convertToJsonString(createSampleRequest(Arrays.asList("sample_user3"), TEST_TEMPLATE_NAME)))
                .run();
    }

    /*
     * Tests: GET /foundation/hellosign/{id}
     */
    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "test-role")
    void getHellosignRequestById_success() {
        EndpointTestBuilder.from(GET_SINGLE_SUCCESS).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "test-role")
    void getHellosignRequestById_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_SINGLE_SUCCESS).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "test-role")
    void getHellosignRequestById_success_matchingSpec() {
        EndpointTestBuilder.from(GET_SINGLE_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getHellosignRequestById_failure_noAuth() {
        EndpointTestBuilder.from(GET_SINGLE_AUTH_REQUIRED).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "test-role")
    void getHellosignRequestById_failure_noMatchingRequest() {
        EndpointTestBuilder.from(GET_SINGLE_REQUEST_NOT_FOUND).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "test-role")
    void getHellosignRequestById_failure_invalidRequestId() {
        EndpointTestBuilder.from(GET_SINGLE_REQUEST_INVALID_ID).run();
    }

    /*
     * Tests: GET /foundation/hellosign/{id}/document
     */
    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "test-role")
    void getHellosignRequestDocumentById_success() {
        EndpointTestBuilder.from(GET_DOCUMENT_SUCCESS).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "test-role")
    void getHellosignRequestDocumentById_success_validResponseFormat() {
        EndpointTestBuilder.from(GET_DOCUMENT_SUCCESS).andCheckFormat().run();
    }

    @Test
    void getHellosignRequestDocumentById_failure_authRequired() {
        EndpointTestBuilder.from(GET_DOCUMENT_AUTH_REQUIRED).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "test-role")
    void getHellosignRequestDocumentById_failure_noMatchingRequest() {
        EndpointTestBuilder.from(GET_DOCUMENT_NOT_FOUND).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "test-role")
    void getHellosignRequestDocumentById_failure_invalidRequestId() {
        EndpointTestBuilder.from(GET_DOCUMENT_INVALID_ID).run();
    }

    /*
     * Internal helper methods
     */

    /**
     * Create a sample request to create a Hellosign signature with only signers and template ID set.
     * 
     * @param signers the signers to assign to the document
     * @param templateId the Hellosign template ID to use to form request
     * @return the prepared Hellosign request
     */
    private HellosignSignatureRequest createSampleRequest(List<String> signers, String templateId) {
        return createSampleRequest(signers, "", "", templateId);
    }

    /**
     * Create a fully custom Hellosign signature request body.
     * 
     * @param signers the signers to set into the request
     * @param message email message to attach to the signature request
     * @param subject email subject to pass for the signature request
     * @param templateId Hellosign template ID to target with request
     * @return the prepared Hellosign signature request body
     */
    private HellosignSignatureRequest createSampleRequest(List<String> signers, String message, String subject,
            String templateId) {
        return HellosignSignatureRequest
                .builder()
                .setMessage(message)
                .setSubject(subject)
                .setSigners(signers
                        .stream()
                        .map(s -> Signer
                                .builder()
                                .setFullname("Test test")
                                .setName(s)
                                .setRole("Client")
                                .setMail(s + "@eclipse-foundation.org")
                                .build())
                        .toList())
                .setTemplateName(templateId)
                .build();
    }

    /**
     * Required for body of requests in tests, as RESTassured doesn't respect the config set for how data is serialized.
     * 
     * @param o the object to serialize
     * @return the serialized JSON string
     */
    private String convertToJsonString(Object o) {
        try {
            return mapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
        }
        return null;
    }
}
