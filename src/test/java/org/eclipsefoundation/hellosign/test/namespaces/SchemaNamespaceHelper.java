/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.test.namespaces;

/**
 * @author martin
 *
 */
public class SchemaNamespaceHelper {
    public static final String BASE_SCHEMA_PATH = "schemas/";
    public static final String BASE_SCHEMA_PATH_SUFFIX = "-schema.json";

    public static final String REQUESTS_SCHEMA_PATH = BASE_SCHEMA_PATH + "ef-hellosign-requests"
            + BASE_SCHEMA_PATH_SUFFIX;
    public static final String REQUEST_SCHEMA_PATH = BASE_SCHEMA_PATH + "ef-hellosign-request"
            + BASE_SCHEMA_PATH_SUFFIX;
}
