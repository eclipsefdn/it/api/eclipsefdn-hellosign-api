/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.test.api;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysModLogData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationDataBuilder;
import org.eclipsefoundation.hellosign.apis.FoundationDbAPI;
import org.jboss.resteasy.reactive.RestResponse;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Priority;
import jakarta.enterprise.inject.Alternative;
import jakarta.inject.Singleton;

/**
 * @author Martin Lowe
 *
 */
@Alternative
@Priority(1)
@RestClient
@Singleton
public class MockFoundationDbApi implements FoundationDbAPI {

    public static final String PEARSON_PERSON_ID = "opearson";

    // make the test storage visible to test for presence of values more easily
    public List<PeopleDocumentData> pplDocs;
    public List<OrganizationDocumentData> orgDocs;
    public List<PeopleData> ppl;
    public List<FullOrganizationContactData> orgConacts;
    public List<SysModLogData> modLogs;

    @PostConstruct
    void init() {
        this.ppl = new ArrayList<>();
        this.ppl
                .add(PeopleDataBuilder
                        .builder()
                        .email("oli.pearson@eclipse.org")
                        .personID(PEARSON_PERSON_ID)
                        .fname("first name")
                        .lname("last name")
                        .type("member")
                        .member(false)
                        .unixAcctCreated(false)
                        .issuesPending(false)
                        .build());
        this.pplDocs = new ArrayList<>();
        this.orgConacts = new ArrayList<>();
        this.orgConacts
                .add(FullOrganizationContactDataBuilder
                        .builder()
                        .person(this.ppl.get(0))
                        .sysRelation(SysRelationDataBuilder.builder().active(false).description("").relation("CR").type("CO").build())
                        .title("")
                        .documentID("")
                        .build());
        this.orgDocs = new ArrayList<>();
        this.modLogs = new ArrayList<>();
    }

    @Override
    public RestResponse<List<FullOrganizationContactData>> getOrganizationContact(BaseAPIParameters params, String username,
            String relation, Integer organizationId) {
        return RestResponse
                .ok(orgConacts
                        .stream()
                        .filter(oc -> username == null || oc.person().personID().equals(username))
                        .filter(oc -> relation == null || oc.sysRelation().relation().equals(relation))
                        .filter(oc -> organizationId == null || oc.organization().organizationID() == organizationId)
                        .toList());
    }

    @Override
    public RestResponse<List<OrganizationDocumentData>> getOrganizationDocuments(BaseAPIParameters params, Integer organizationId,
            List<String> documentIds) {
        return RestResponse
                .ok(this.orgDocs
                        .stream()
                        .filter(od -> organizationId == null || od.organizationID() == organizationId)
                        .filter(od -> documentIds == null || documentIds.isEmpty() || documentIds.contains(od.documentID()))
                        .toList());
    }

    @Override
    public PeopleData getPersonRecord(String username) {
        return this.ppl.stream().filter(p -> p.personID().equals(username)).findFirst().orElse(null);
    }

    @Override
    public RestResponse<List<PeopleDocumentData>> getPeopleDocuments(BaseAPIParameters params, List<String> docIds,
            List<String> peopleIds) {
        return RestResponse
                .ok(this.pplDocs
                        .stream()
                        .filter(pd -> peopleIds == null || peopleIds.isEmpty() || peopleIds.contains(pd.personID()))
                        .filter(pd -> docIds == null || docIds.isEmpty() || docIds.contains(pd.documentID()))
                        .toList());
    }

    @Override
    public RestResponse<List<PeopleDocumentData>> getPeopleDocumentsForUser(BaseAPIParameters params, String username,
            List<String> docIds) {
        return RestResponse
                .ok(this.pplDocs
                        .stream()
                        .filter(pd -> username == null || username.equals(pd.personID()))
                        .filter(pd -> docIds == null || docIds.isEmpty() || docIds.contains(pd.documentID()))
                        .toList());
    }

    @Override
    public RestResponse<List<PeopleDocumentData>> updatePeopleDocument(String username, PeopleDocumentData payload) {
        this.pplDocs.removeIf(pd -> pd.documentID().equals(payload.documentID()) && pd.personID().equals(username));
        this.pplDocs.add(payload);
        return RestResponse.ok(List.of(payload));
    }

    @Override
    public RestResponse<OrganizationDocumentData> updateOrganizationDocument(Integer organizationId, OrganizationDocumentData payload) {
        this.orgDocs.removeIf(od -> od.documentID().equals(payload.documentID()) && od.organizationID() == organizationId);
        this.orgDocs.add(payload);
        return RestResponse.ok(payload);
    }

    @Override
    public List<PeopleData> updatePeople(PeopleData payload) {
        this.ppl.removeIf(p -> p.personID().equals(payload.personID()));
        this.ppl.add(payload);
        return List.of(payload);
    }

    @Override
    public List<SysModLogData> updateModlog(SysModLogData payload) {
        this.modLogs.add(payload);
        return List.of(payload);
    }

}
