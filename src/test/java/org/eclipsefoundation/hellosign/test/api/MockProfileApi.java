/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.ProfileAPI;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUserBuilder;
import org.eclipsefoundation.efservices.api.models.EfUserCountryBuilder;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Priority;
import jakarta.enterprise.inject.Alternative;
import jakarta.inject.Singleton;

/**
 * @author martin
 *
 */
@Alternative
@Priority(1)
@RestClient
@Singleton
public class MockProfileApi implements ProfileAPI {

    List<EfUser> users;

    @PostConstruct
    void init() {
        this.users = new ArrayList<>();
        this.users.add(buildBase().name("opearson").mail("oli.pearson@eclipse.org").build());
    }

    @Override
    public List<EfUser> getUsers(String token, UserSearchParams arg1) {
        return users
                .stream()
                .filter(u -> StringUtils.isBlank(arg1.mail) || u.mail().equals(arg1.mail))
                .filter(u -> StringUtils.isBlank(arg1.name) || u.name().equals(arg1.name))
                .filter(u -> arg1.uid == null || u.uid() == arg1.uid)
                .toList();
    }

    private EfUserBuilder buildBase() {
        return EfUserBuilder
                .builder()
                .uid("666")
                .name("firstlast")
                .githubHandle("handle")
                .mail("firstlast@test.com")
                .picture("pic url")
                .firstName("fake")
                .lastName("user")
                .publisherAgreements(new HashMap<>())
                .twitterHandle("")
                .org("null")
                .jobTitle("employee")
                .website("site url")
                .country(EfUserCountryBuilder.builder().build())
                .interests(Arrays.asList());
    }

    @Override
    public EfUser getUserByEfUsername(String arg0, String arg1) {
        return users.stream().filter(u -> u.name().equals(arg1)).findFirst().orElse(null);
    }

    @Override
    public EfUser getUserByGithubHandle(String arg0, String arg1) {
        return users.stream().filter(u -> u.githubHandle().equals(arg1)).findFirst().orElse(null);
    }

}
