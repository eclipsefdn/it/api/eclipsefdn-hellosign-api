/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.dtos.eclipse;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.eclipsefoundation.hellosign.daos.EclipsePersistenceDao;
import org.eclipsefoundation.hellosign.dtos.eclipse.AccountRequests.AccountRequestsCompositeId;
import org.eclipsefoundation.hellosign.namespaces.SignatureAPIParameterNames;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * 
 */
@QuarkusTest
class AccountRequestsTest {

    @Inject
    FilterService filters;
    @Inject
    EclipsePersistenceDao dao;

    @Test
    void hasFilter() {
        Assertions.assertNotNull(filters.get(AccountRequests.class));
    }

    @Test
    void filter_success_token() {
        String token = UUID.randomUUID().toString();
        AccountRequests sample = getTestInstance(Optional.empty(), Optional.empty(), token);
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org"));
        // set up the request we will be retrieving by query
        List<AccountRequests> out = dao
                .add(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class)), Arrays.asList(sample));
        Assertions.assertFalse(out.isEmpty(), "Basic document creation failed unexpectedly, cannot perform test");
        AccountRequests expected = out.get(0);

        // set up the params for the query to get the expected result
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(SignatureAPIParameterNames.TOKEN_NAME_RAW, token);
        List<AccountRequests> results = dao.get(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class), params));

        Assertions.assertEquals(1, results.size(), "Results should only have 1 result for specific ID filter");
        Assertions.assertEquals(expected, results.get(0), "Returned result should be equivalent to the added document");
    }

    @Test
    void filter_success_email() {
        String email = UUID.randomUUID().toString();
        AccountRequests sample = getTestInstance(Optional.of(email), Optional.empty(), "");
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org"));
        // set up the request we will be retrieving by query
        List<AccountRequests> out = dao
                .add(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class)), Arrays.asList(sample));
        Assertions.assertFalse(out.isEmpty(), "Basic document creation failed unexpectedly, cannot perform test");
        AccountRequests expected = out.get(0);

        // set up the params for the query to get the expected result
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(SignatureAPIParameterNames.EMAIL_NAME_RAW, email);
        List<AccountRequests> results = dao.get(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class), params));

        Assertions.assertEquals(1, results.size(), "Results should only have 1 result for specific ID filter");
        Assertions.assertEquals(expected, results.get(0), "Returned result should be equivalent to the added document");
    }

    @Test
    void filter_success_reqWhen() {
        LocalDateTime targetTime = LocalDateTime.of(2005, 6, 15, 10, 55);
        AccountRequests sample = getTestInstance(Optional.empty(), Optional.of(targetTime), "");
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org"));
        // set up the request we will be retrieving by query
        List<AccountRequests> out = dao
                .add(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class)), Arrays.asList(sample));
        Assertions.assertFalse(out.isEmpty(), "Basic document creation failed unexpectedly, cannot perform test");
        AccountRequests expected = out.get(0);

        // set up the params for the query to get the expected result
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(SignatureAPIParameterNames.REQ_WHEN_NAME_RAW, targetTime.toString());
        List<AccountRequests> results = dao.get(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class), params));

        Assertions.assertEquals(1, results.size(), "Results should only have 1 result for specific ID filter");
        Assertions.assertEquals(expected, results.get(0), "Returned result should be equivalent to the added document");
    }

    private AccountRequests getTestInstance(Optional<String> email, Optional<LocalDateTime> reqWhen, String token) {
        AccountRequestsCompositeId arci = new AccountRequestsCompositeId();
        arci.setEmail(email.orElse(UUID.randomUUID().toString()));
        arci.setReqWhen(reqWhen.orElse(DateTimeHelper.now().toLocalDateTime()));
        AccountRequests test = new AccountRequests();
        test.setId(arci);
        test.setFname("");
        test.setIp("");
        test.setLname("");
        test.setNewEmail("");
        test.setPassword("eclipsecla");
        test.setToken(token);
        return test;
    }
}
