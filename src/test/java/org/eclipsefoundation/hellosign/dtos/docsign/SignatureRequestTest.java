/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.dtos.docsign;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.eclipsefoundation.hellosign.namespaces.HellosignRequestStatus;
import org.eclipsefoundation.hellosign.namespaces.SignatureAPIParameterNames;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * 
 */
@QuarkusTest
class SignatureRequestTest {

    @Inject
    FilterService filters;
    @Inject
    DefaultHibernateDao dao;

    @Test
    void hasFilter() {
        Assertions.assertNotNull(filters.get(SignatureRequest.class));
    }

    @Test
    void filter_success_id() {
        SignatureRequest sample = getTestInstance("", "", Optional.empty());
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org"));
        // set up the request we will be retrieving by query
        List<SignatureRequest> out = dao
                .add(new RDBMSQuery<>(wrap, filters.get(SignatureRequest.class)), Arrays.asList(sample));
        Assertions.assertFalse(out.isEmpty(), "Basic document creation failed unexpectedly, cannot perform test");
        SignatureRequest expected = out.get(0);

        // set up the params for the query to get the expected result
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID_PARAMETER_NAME, Long.toString(expected.getId()));
        List<SignatureRequest> results = dao.get(new RDBMSQuery<>(wrap, filters.get(SignatureRequest.class), params));

        Assertions.assertEquals(1, results.size(), "Results should only have 1 result for specific ID filter");
        Assertions.assertEquals(expected, results.get(0), "Returned result should be equivalent to the added document");
    }

    @Test
    void filter_success_template() {
        String uniqueTemplateName = UUID.randomUUID().toString();
        SignatureRequest sample = getTestInstance("", uniqueTemplateName, Optional.empty());
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org"));
        // set up the request we will be retrieving by query
        List<SignatureRequest> out = dao
                .add(new RDBMSQuery<>(wrap, filters.get(SignatureRequest.class)), Arrays.asList(sample));
        Assertions.assertFalse(out.isEmpty(), "Basic document creation failed unexpectedly, cannot perform test");
        SignatureRequest expected = out.get(0);

        // set up the params for the query to get the expected result
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(SignatureAPIParameterNames.TEMPLATE_NAME_RAW, uniqueTemplateName);
        List<SignatureRequest> results = dao.get(new RDBMSQuery<>(wrap, filters.get(SignatureRequest.class), params));

        Assertions.assertEquals(1, results.size(), "Results should only have 1 result for specific ID filter");
        Assertions.assertEquals(expected, results.get(0), "Returned result should be equivalent to the added document");
    }

    private SignatureRequest getTestInstance(String requestId, String templateId,
            Optional<HellosignRequestStatus> status) {
        SignatureRequest test = new SignatureRequest();
        test.setCreated(DateTimeHelper.now());
        test.setMessage("");
        test.setRequestId(requestId);
        test.setRequestTemplateId(templateId);
        test.setStatus(status.orElse(HellosignRequestStatus.ECLIPSE_API_HELLOSIGN_REQUEST_STATUS_UNFINISHED));
        test.setSubject("");
        test.setTitle("");
        test.setUpstreamResponse("");
        return test;
    }
}
