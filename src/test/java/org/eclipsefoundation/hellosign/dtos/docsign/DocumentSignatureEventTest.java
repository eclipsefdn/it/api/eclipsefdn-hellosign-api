/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.hellosign.dtos.docsign;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.eclipsefoundation.hellosign.namespaces.SignatureAPIParameterNames;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.dropbox.sign.model.EventCallbackRequestEvent.EventTypeEnum;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * 
 */
@QuarkusTest
class DocumentSignatureEventTest {

    @Inject
    FilterService filters;
    @Inject
    DefaultHibernateDao dao;

    @Test
    void hasFilter() {
        Assertions.assertNotNull(filters.get(DocumentSignatureEvent.class));
    }

    @Test
    void filter_success_id() {
        DocumentSignatureEvent sample = getTestInstance("", Optional.empty());
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org"));
        // set up the request we will be retrieving by query
        List<DocumentSignatureEvent> out = dao
                .add(new RDBMSQuery<>(wrap, filters.get(DocumentSignatureEvent.class)), Arrays.asList(sample));
        Assertions.assertFalse(out.isEmpty(), "Basic document creation failed unexpectedly, cannot perform test");
        DocumentSignatureEvent expected = out.get(0);

        // set up the params for the query to get the expected result
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID_PARAMETER_NAME, Long.toString(expected.getId()));
        List<DocumentSignatureEvent> results = dao
                .get(new RDBMSQuery<>(wrap, filters.get(DocumentSignatureEvent.class), params));

        Assertions.assertEquals(1, results.size(), "Results should only have 1 result for specific ID filter");
        Assertions.assertEquals(expected, results.get(0), "Returned result should be equivalent to the added document");
    }

    @Test
    void filter_success_requestId() {
        String requestId = UUID.randomUUID().toString();
        DocumentSignatureEvent sample = getTestInstance(requestId, Optional.empty());
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org"));
        // set up the request we will be retrieving by query
        List<DocumentSignatureEvent> out = dao
                .add(new RDBMSQuery<>(wrap, filters.get(DocumentSignatureEvent.class)), Arrays.asList(sample));
        Assertions.assertFalse(out.isEmpty(), "Basic document creation failed unexpectedly, cannot perform test");
        DocumentSignatureEvent expected = out.get(0);

        // set up the params for the query to get the expected result
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(SignatureAPIParameterNames.UPSTREAM_REQUEST_ID_RAW, requestId);
        List<DocumentSignatureEvent> results = dao
                .get(new RDBMSQuery<>(wrap, filters.get(DocumentSignatureEvent.class), params));

        Assertions.assertEquals(1, results.size(), "Results should only have 1 result for specific request ID filter");
        Assertions.assertEquals(expected, results.get(0), "Returned result should be equivalent to the added document");
    }

    @Test
    void filter_success_eventType() {
        String eventType = EventTypeEnum.SIGNATURE_REQUEST_VIEWED.getValue();
        DocumentSignatureEvent sample = getTestInstance("", Optional.of(eventType));
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("http://eclipse.org"));
        // set up the request we will be retrieving by query
        List<DocumentSignatureEvent> out = dao
                .add(new RDBMSQuery<>(wrap, filters.get(DocumentSignatureEvent.class)), Arrays.asList(sample));
        Assertions.assertFalse(out.isEmpty(), "Basic document creation failed unexpectedly, cannot perform test");
        DocumentSignatureEvent expected = out.get(0);

        // set up the params for the query to get the expected result
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        // need ID as there are other documents in persistence in session
        params.add(DefaultUrlParameterNames.ID_PARAMETER_NAME, Long.toString(expected.getId()));
        params.add(SignatureAPIParameterNames.EVENT_TYPE_NAME_RAW, eventType);
        List<DocumentSignatureEvent> results = dao
                .get(new RDBMSQuery<>(wrap, filters.get(DocumentSignatureEvent.class), params));

        Assertions.assertEquals(1, results.size(), "Results should only have 1 result for specific request ID filter");
        Assertions.assertEquals(expected, results.get(0), "Returned result should be equivalent to the added document");
    }

    private DocumentSignatureEvent getTestInstance(String requestId, Optional<String> eventType) {
        DocumentSignatureEvent test = new DocumentSignatureEvent();
        test.setCreated(DateTimeHelper.now());
        test.setEventBody("");
        test.setRequestId(requestId);
        test.setEventType(eventType.orElse(EventTypeEnum.SIGNATURE_REQUEST_SENT.getValue()));
        return test;
    }
}
