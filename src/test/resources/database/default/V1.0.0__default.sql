
CREATE TABLE `hellosign_request` (
  `hid` bigint auto_increment COMMENT 'Primary key of the request entity.',
  `request_template_id` varchar(255) NOT NULL DEFAULT 0 COMMENT 'The template id used for the document.',
  `request_title` varchar(255) DEFAULT NULL COMMENT 'The  title of the  document.',
  `request_subject` varchar(255) DEFAULT NULL COMMENT 'The subject of the document.',
  `request_message` longtext DEFAULT NULL COMMENT 'The message attached to the document.',
  `signature_request_id` varchar(255) NOT NULL COMMENT 'The id for an overall signature request.',
  `response` longblob NOT NULL COMMENT 'The content of the response from hellosign.',
  `created` int NOT NULL COMMENT 'The Unix timestamp when the request was created.',
  request_status bigint NOT NULL,
  PRIMARY KEY (`hid`)
);

INSERT INTO hellosign_request(request_template_id,request_title,request_subject,request_message,signature_request_id,response,created,request_status)
  VALUES('1','sample title','subject','message','request-id','',1686157155,0);

INSERT INTO hellosign_request(request_template_id,request_title,request_subject,request_message,signature_request_id,response,created,request_status)
  VALUES('1','sample title','subject','message','completion-test-1','',1686157155,0);

CREATE TABLE `hellosign_signer` (
  `sid` bigint auto_increment COMMENT 'Primary key of the signer.',
  `signature_request_id` varchar(255) NOT NULL COMMENT 'The id for an overall signature request.',
  `signer_role` varchar(255) NOT NULL COMMENT 'The signer_role of the signer for this document request.',
  `signer_mail` varchar(255) NOT NULL COMMENT 'The signer_mail of the signer.',
  `signer_name` varchar(255) NOT NULL COMMENT 'The full signer_name of the signer.',
  `signer_person_id` varchar(255) DEFAULT NULL COMMENT 'The identification used in the foundation database.',
  `created` int NOT NULL DEFAULT 0 COMMENT 'The Unix timestamp when the blob was created.',
  PRIMARY KEY (`sid`)
);

INSERT INTO hellosign_signer(signature_request_id, signer_role, signer_mail, signer_name, signer_person_id, created)
  VALUES('completion-test-1', 'client', 'test@test.com', 'Oli Pearson', 'opearson', 1686157155);
INSERT INTO hellosign_signer(signature_request_id, signer_role, signer_mail, signer_name, signer_person_id, created)
  VALUES('completion-test-1', 'cosigner', 'someother@test.com', 'Other Person', 'someguy', 1686157155);

CREATE TABLE `hellosign_event` (
  `eid` bigint auto_increment COMMENT 'Primary key of the event entity.',
  `event_type` varchar(255) NOT NULL COMMENT 'The type of hellosign event being logged.',
  `signature_request_id` varchar(255) NOT NULL COMMENT 'The id for an overall signature request.',
  `body` longblob NOT NULL COMMENT 'The content of the body.',
  `created` int NOT NULL COMMENT 'The Unix timestamp when the blob was created.',
  PRIMARY KEY (`eid`)
);